# CLEAR beamline optics
This repository contains the MadX model of the CLEAR beamlines.

The main model is contained in the CLEAR.seqx file, which describes the various sequences used.

The folder `surveyFiles` is contains the madx script to run the survey commands, as well as tables of element positions in TFS format.
Please remember that in the survey tables, the element s-position is at the end of the element, and that the element lengths are their magnetic lenghts as described by the "length" column.
Tables are given in the global CERN coordinate system (`survey`), in a local coordinate system starting at the gun (`survet0`), and in a local coordinate system with only the most relevant elements included (`survey0_filtered`).

The folder `scripts` contains various example scripts for matching the optics in CLEAR.

## Generic GIT commands:
The repository is managed by git, enabling version-control in an easy-to access central repository.
A summary of some basic git commands are given below.
If you choose, you can also use a graphical client or directly download files from the repository web-page.

- download the project 
```
git clone https://:@gitlab.cern.ch:8443/CLEAR/CLEARLattice.git
```

- download a specific (2018) branch (normally working on a specific year branch)
```
git clone -b 2018 https://:@gitlab.cern.ch:8443/CLEAR/CLEARLattice.git
```

- update the local repository with the remote repository
```
git pull
```

- add a file
```
git add newFile.txt
```

- commit the changes to the LOCAL repository
```
git commit -a -m "my comment"
```
note that the -a option means "all", i.e. it commits all files that where added (see procedure above) and the ones that were already in the repository but you modified.

- synchronise the LOCAL repository to the REMOTE repository
```
git push
```

