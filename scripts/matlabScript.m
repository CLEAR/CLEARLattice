%
%
% simple script to use CLEAR model with madx2matlab libraries
%

% important paths
myPath  = '/Users/davideg/CERN/CLEAR/CLEARLattice/scripts';
madx2matlabPath = '/Users/davideg/CERN/work/madx2MatlabLib';
% custom compiled MAD-X:
madXPath = '/Users/davideg/madx/source/madx-5.03.06/build/madx64';
%madXPath = '/Users/davideg/madx/source/madx-5.03.06-macosx64-gnu';

%
% start initialising env.
addpath(madx2matlabPath);
cd(myPath);

%% start with script
clearScript = fullfile(myPath,'matlabScript.madx');


myClearModel = madx2matlab(clearScript,'',madXPath);
myClearModel.twissCommandLine = {...
    'use, period=CA.VESPER, RANGE=CA.QFD0350/#E;' % to use vesper instead
    'select, flag=twiss, clear;';...
    'select, flag=twiss, column=name, KEYWORD, S, L, K2L, K1L, K0L, MUX, MUY, KMAX, BETX, ALFX, X, PX, DX, BETY, ALFY, Y, PY, DY, T, PT, DPX, DPY, APERTYPE, APER_1, APER_2;';...
    'twiss, betx=CA.IBETX, bety=CA.IBETY, alfx=CA.IALFX, ALFY=CA.IALFY, deltap=CA.IDP;';...
    };
myClearModel.madxFormat = '22.15e';

%% set those initi conditions.
myClearModel.setModelParameters('CA.IBETX', 14.);
myClearModel.setModelParameters('CA.IALFX', 0);
myClearModel.setModelParameters('CA.IBETY', 14.);
myClearModel.setModelParameters('CA.IALFY', 0);
myClearModel.setModelParameters('CA.EN', 200);


%% compute optics
myClearOptics = myClearModel.computeOptics();

%% plot it
figure(1)
clf
madx2matlab.plotMachineTwissFunctions(myClearOptics,1,true);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Other optics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% optics for 10 m inAir
myClearModel.setModelParameters('ca.iqfd0350',  4.560027211);
myClearModel.setModelParameters('ca.iqdd0355',  3.582647345) ;
myClearModel.setModelParameters('ca.iqfd0360',  0.345613132) ;
myClearModel.setModelParameters('ca.iqfd0510',  9.950016904) ;
myClearModel.setModelParameters('ca.iqdd0515',  50.48643069) ;
myClearModel.setModelParameters('ca.iqfd0520',  44.91429832) ;
myClearModel.setModelParameters('ca.iqfd0760',  46.32933723) ;
myClearModel.setModelParameters('ca.iqdd0765',  69.09083627) ;
myClearModel.setModelParameters('ca.iqfd0770',  29.84565669) ;
myClearOpticsGentle = myClearModel.computeOptics();

%% optics for 0.1 m inAir
myClearModel.setModelParameters('ca.iqfd0350',   0.02121011925) ;
myClearModel.setModelParameters('ca.iqdd0355',     5.213079655) ;
myClearModel.setModelParameters('ca.iqfd0360',  0.003074998957) ;
myClearModel.setModelParameters('ca.iqfd0510',     1.542682967) ;
myClearModel.setModelParameters('ca.iqdd0515',     51.50127215) ;
myClearModel.setModelParameters('ca.iqfd0520',     53.39451433) ;
myClearModel.setModelParameters('ca.iqfd0760',      45.4042432) ;
myClearModel.setModelParameters('ca.iqdd0765',      80.9788221) ;
myClearModel.setModelParameters('ca.iqfd0770',     46.33638226) ;
myClearOpticsStrong = myClearModel.computeOptics();

%% optics for 0.1 m plasma cell
myClearModel.setModelParameters('ca.iqfd0350',   50.94111722 ) ;
myClearModel.setModelParameters('ca.iqdd0355',   51.15212589 ) ;
myClearModel.setModelParameters('ca.iqfd0360',   9.594445443 ) ;
myClearModel.setModelParameters('ca.iqfd0510',   46.51290937 ) ;
myClearModel.setModelParameters('ca.iqdd0515',    39.1006432 ) ;
myClearModel.setModelParameters('ca.iqfd0520',   7.480360187 ) ;
myClearModel.setModelParameters('ca.iqfd0760',   61.41000725 ) ;
myClearModel.setModelParameters('ca.iqdd0765',   105.7478405 ) ;
myClearModel.setModelParameters('ca.iqfd0770',   59.94622818 ) ;
myClearOpticsPlasma = myClearModel.computeOptics();


%% optics for 0.1 m plasma cell a bit more uniform
myClearModel.setModelParameters('ca.iqfd0350',   53.66132231  ) ;
myClearModel.setModelParameters('ca.iqdd0355',   62.65632861  ) ;
myClearModel.setModelParameters('ca.iqfd0360',   28.96964837  ) ;
myClearModel.setModelParameters('ca.iqfd0510',   45.00877784  ) ;
myClearModel.setModelParameters('ca.iqdd0515',   50.06379694  ) ;
myClearModel.setModelParameters('ca.iqfd0520',   13.34622562  ) ;
myClearModel.setModelParameters('ca.iqfd0760',   65.94684321  ) ;
myClearModel.setModelParameters('ca.iqdd0765',   83.10915034  ) ;
myClearModel.setModelParameters('ca.iqfd0770',   7.880140179  ) ;
myClearOpticsPlasmaUniform = myClearModel.computeOptics();



%% optics for small beta at vesper
myClearModel.setModelParameters('ca.iqfd0350',   32.09208176  ) ;
myClearModel.setModelParameters('ca.iqdd0355',   72.00874906  ) ;
myClearModel.setModelParameters('ca.iqfd0360',   47.12374905  ) ;
myClearOpticsVESPER = myClearModel.computeOptics();




%% plot them all
myEnergy = 200; % MeV
myEmittance = 10; %\mu m 
myClearAllOptics = {
    myClearOpticsGentle;...
    myClearOpticsStrong;...
    myClearOpticsPlasma;...
    ...%myClearOpticsPlasmaUniform;...
    };
myClearOpticsTitles = {...
    'CLIC',...
    'In-Air',...
    'Plasma lens',...
    ...'',...
    };
myClearAllOptics = {
    myClearOpticsVESPER;
    };
myClearOpticsTitles = {...
    'VESPER',...
    };

%% prepare figure
figure(1)
clf
output_axes = madx2matlab.plotMachineTwissFunctions(myClearAllOptics{1},1,true);
%% do the actual plots...
cla(output_axes(2));
hold all
for i=1:length(myClearAllOptics)
    myClearOptics = myClearAllOptics{i};
    
    %
    myS = [myClearOptics.DATA.S];
    myBetX = [myClearOptics.DATA.BETX];
    myBetY = [myClearOptics.DATA.BETY];
    mySigmaX = sqrt(myBetX*myEmittance*0.511/myEnergy);
    mySigmaY = sqrt(myBetY*myEmittance*0.511/myEnergy);
    %mySigmaX = [myClearOptics.DATA.BETX];
    %mySigmaY = [myClearOptics.DATA.BETY];
    %
    cip = plot(output_axes(2), myS, mySigmaX, 'LineWidth', 2);
    plot(output_axes(2), myS, mySigmaY, '--', 'LineWidth', 2, 'Color', cip.Color);
end
%
ylabel(output_axes(2), '\sigma [mm]')
aux = get(output_axes(2), 'Children');
legend(aux(2:2:end), myClearOpticsTitles(end:-1:1),  'location', 'best')
grid(output_axes(2),  'off')
%
%ylim(output_axes(2), [0 1.5])
xlim(output_axes(2), [0, 18])

%set(output_axes(1), 'FontSize',20)
%set(output_axes(2), 'FontSize',20)