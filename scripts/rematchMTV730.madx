!
! Script to rematch beta functions at MTV 0730 (i.e. after CLIC module)
!
! Sept 2017 - davide 

! build sequence and load nominal currents
call, file="../clear.seqx";
call, file="../nominalClear.txt";

! re-set init values
CA.IBETX = 13.4;
CA.IALFX = -1.5;
CA.IBETY = 18.9;
CA.IALFY = -1.4;
CA.EN = 171;

! values Nov 2017
CA.IBETX = 21.7;
CA.IALFX = -0.03;
CA.IBETY = 20.4;
CA.IALFY = 0.27;
CA.EN = 180;


!!!!
! beam definition
!!!!
beam,particle=electron,energy=CA.EN/1000., EX=NEX/CA.EN/0.511, EY=NEY/CA.EN/0.511, ET=1/1000, SIGT=0, SIGE=sige;

!!!!
! define the period to use
!!!!
use, period=CA.STLINE, RANGE=CA.QFD0350/#E;


!!!
! plot nominal optics
!!!
twiss, betx=CA.IBETX, bety=CA.IBETY, alfx=CA.IALFX, ALFY=CA.IALFY;
plot, noversion=true, table=twiss, haxis=s, vaxis1=betx,bety, interpolate,
     title="CLEAR optics", colour=100;

! optimise for low beta
! my optimisation
match, use_macro;
      ! NOTE: currently magnets can only be powered with positive currents
      !       but there is no reason not to invert the polarity... 
      vary, name=CA.IQFD0350, step=1.2, lower= 0.0, upper= 200;
      vary, name=CA.IQDD0355, step=1.2, lower= 0.0, upper= 200;
      vary, name=CA.IQFD0360, step=1.2, lower= 0.0, upper= 200;
      vary, name=CA.IQFD0510, step=1.2, lower= 0.0, upper= 200;
      vary, name=CA.IQDD0515, step=1.2, lower= 0.0, upper= 200;
      vary, name=CA.IQFD0520, step=1.2, lower= 0.0, upper= 200;
      !vary, name=CA.IQFD0760, step=1.2, lower= 0.0, upper= 200;
      !vary, name=CA.IQDD0765, step=1.2, lower= 0.0, upper= 200;
      !vary, name=CA.IQFD0770, step=1.2, lower= 0.0, upper= 200;


      m11: macro=
       {
         use, period=CA.STLINE, RANGE=CA.QFD0350/#E;
         twiss, betx=CA.IBETX, bety=CA.IBETY, alfx=CA.IALFX, ALFY=CA.IALFY;
       };

      !!!!! possible example of constraints
      !
      !
      !! min bet* at ACS0640
      constraint, weight=1000, expr=table(twiss, CA.ACS0645, betx) < 100; 
      constraint, weight=1000, expr=table(twiss, CA.ACS0645, bety) < 100; 
      !
      !! min beta* at CA.PLC0780S
      constraint, weight=10000000, expr=table(twiss, CA.PLC0800S, betx) < 40; 
      constraint, weight=10000000, expr=table(twiss, CA.PLC0800S, bety) < 40; 
      !constraint, weight=10000000, expr=table(twiss, CA.PLC0800S, alfx) = 0; 
      !constraint, weight=10000000, expr=table(twiss, CA.PLC0800S, alfy) = 0; 
      !
      !! min beta* at CA.MTV0730
      constraint, weight=10000, expr=table(twiss, CA.MTV0730, betx) < 20; 
      constraint, weight=10000, expr=table(twiss, CA.MTV0730, bety) < 20; 
      constraint, weight=10000, expr=table(twiss, CA.MTV0730, alfx) = 0; 
      constraint, weight=10000, expr=table(twiss, CA.MTV0730, alfy) = 0; 
      !

      !!! cosmetics -> not to explode betas
      ! typical normalised emittance = 10 um.
      !  -> beam pipe ~ 2.5 cm radius
      !  -> this correspond to about 25 km beta
      !constraint, weight=10, betx < 1000;
      !constraint, weight=10, bety < 1000;
      constraint, weight=10, expr=table(summ,betxmax) < 1000;
      constraint, weight=10, expr=table(summ,betymax) < 1000;


     !!!! minimisation method !!!!!!!!!!!!!!!
      lmdif,   tolerance:=1e-06, calls:=10000;
!      simplex,  tolerance:=1e-09, calls:=1000;
!      lmdif,    tolerance:=1e-09, calls:=1000;
!      jacobian, tolerance:=1e-09, calls:=1000;

endmatch;

!
! final settings
use, period=CA.STLINE, RANGE=CA.QFD0350/#E;
twiss, betx=CA.IBETX, bety=CA.IBETY, alfx=CA.IALFX, ALFY=CA.IALFY, deltap=0.0;
plot, noversion=true, table=twiss, haxis=s, vaxis1=betx,bety, interpolate,
     title="Optimised", colour=100;
!twiss, betx=CA.IBETX, bety=CA.IBETY, alfx=CA.IALFX, ALFY=CA.IALFY, deltap=-0.03;
!plot, noversion=true, table=twiss, haxis=s, vaxis1=betx,bety, interpolate,
!     title="Optimised deltap=-0.03", colour=100;
!twiss, betx=CA.IBETX, bety=CA.IBETY, alfx=CA.IALFX, ALFY=CA.IALFY, deltap=+0.03;
!plot, noversion=true, table=twiss, haxis=s, vaxis1=betx,bety, interpolate,
!     title="Optimised deltap=+0.03", colour=100;

! interesting betas
value, table(twiss, CA.ACS0645, betx);
value, table(twiss, CA.ACS0645, bety);
value, table(twiss, CA.ACS0645, alfx);
value, table(twiss, CA.ACS0645, alfy);
!
value, table(twiss, CA.PLC0800M, betx);
value, table(twiss, CA.PLC0800M, bety);
value, table(twiss, CA.PLC0800M, alfx);
value, table(twiss, CA.PLC0800M, alfy);
!
value, table(twiss, CA.MTV0730, betx);
value, table(twiss, CA.MTV0730, bety);
value, table(twiss, CA.MTV0730, alfx);
value, table(twiss, CA.MTV0730, alfy);


! and quadrupoles currents
value, CA.IQFD0350,
       CA.IQDD0355,
       CA.IQFD0360,
       CA.IQFD0510,
       CA.IQDD0515,
       CA.IQFD0520,
       CA.IQFD0760,
       CA.IQDD0765,
       CA.IQFD0770;

