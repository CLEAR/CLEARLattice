!
! Script to rematch beta functions on VESPER
!
! Sept 2017 - davide 

! build sequence and load nominal currents
call, file="../clear.seqx";
call, file="../nominalClear.txt";

! some standard init values
CA.EN = 200;
CA.IBETX =  14.; 
CA.IALFX =  0; 
CA.IBETY =  14.; 
CA.IALFY =  0; 


!!!!
! beam definition
!!!!
beam,particle=electron,energy=CA.EN/1000., EX=NEX/CA.EN/0.511, EY=NEY/CA.EN/0.511, ET=1/1000, SIGT=0, SIGE=sige;

!!!!
! define the period to use
!!!!
use, period=CA.VESPER, RANGE=CA.QFD0350/#E;


!!!
! plot nominal optics
!!!
twiss, betx=CA.IBETX, bety=CA.IBETY, alfx=CA.IALFX, ALFY=CA.IALFY;
plot, noversion=true, table=twiss, haxis=s, vaxis1=betx,bety, interpolate,
     title="CLEAR optics", colour=100;

! optimise for low beta
! my optimisation
match, use_macro;
      ! NOTE: currently magnets can only be powered with positive currents
      !       but there is no reason not to invert the polarity... 
      vary, name=CA.IQFD0350, step=1.2, lower= -100.0, upper= 200;
      vary, name=CA.IQDD0355, step=1.2, lower= -100.0, upper= 200;
      vary, name=CA.IQFD0360, step=1.2, lower= -100.0, upper= 200;


      m11: macro=
       {
         use, period=CA.VESPER, RANGE=CA.QFD0350/#E;
         twiss, betx=CA.IBETX, bety=CA.IBETY, alfx=CA.IALFX, ALFY=CA.IALFY;
       };

      !!!!! possible example of constraints
      !
      !
      !! min bet* at CAS.ICT0430
      constraint, weight=1000, expr=table(twiss, CAS.ICT0430, betx) = 0.5; 
      constraint, weight=1000, expr=table(twiss, CAS.ICT0430, bety) = 0.5; 
      constraint, weight=100, expr=table(twiss, CAS.ICT0430, alfx) = 0; 
      constraint, weight=100, expr=table(twiss, CAS.ICT0430, alfy) = 0; 

      !!! cosmetics -> not to explode betas
      ! typical normalised emittance = 10 um.
      !  -> beam pipe ~ 2.5 cm radius
      !  -> this correspond to about 25 km beta
      !constraint, weight=10, betx < 1000;
      !constraint, weight=10, bety < 1000;
      constraint, weight=10, expr=table(summ,betxmax) < 1000;
      constraint, weight=10, expr=table(summ,betymax) < 1000;


     !!!! minimisation method !!!!!!!!!!!!!!!
      lmdif,   tolerance:=1e-06, calls:=10000;
!      simplex,  tolerance:=1e-09, calls:=1000;
!      lmdif,    tolerance:=1e-09, calls:=1000;
!      jacobian, tolerance:=1e-09, calls:=1000;

endmatch;

!
! final settings
use, period=CA.VESPER, RANGE=CA.QFD0350/#E;
twiss, betx=CA.IBETX, bety=CA.IBETY, alfx=CA.IALFX, ALFY=CA.IALFY, deltap=0.0;
plot, noversion=true, table=twiss, haxis=s, vaxis1=betx,bety, interpolate,
     title="Optimised", colour=100;
!twiss, betx=CA.IBETX, bety=CA.IBETY, alfx=CA.IALFX, ALFY=CA.IALFY, deltap=-0.03;
!plot, noversion=true, table=twiss, haxis=s, vaxis1=betx,bety, interpolate,
!     title="Optimised deltap=-0.03", colour=100;
!twiss, betx=CA.IBETX, bety=CA.IBETY, alfx=CA.IALFX, ALFY=CA.IALFY, deltap=+0.03;
!plot, noversion=true, table=twiss, haxis=s, vaxis1=betx,bety, interpolate,
!     title="Optimised deltap=+0.03", colour=100;

! interesting betas
value, table(twiss, CAS.ICT0430, betx);
value, table(twiss, CAS.ICT0430, bety);
value, table(twiss, CAS.ICT0430, alfx);
value, table(twiss, CAS.ICT0430, alfy);
!
value, table(twiss, CAS.BTV0420, alfx);
value, table(twiss, CAS.BTV0420, alfy);


! and quadrupoles currents
value, CA.IQFD0350,
       CA.IQDD0355,
       CA.IQFD0360;

